import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import CadastroPaciente from './pages/CadastroPaciente'
import Login from './pages/Login'

import PaginaInicial from './pages/PaginaInicial'
import EsqueciSenha from './pages/EsqueciSenha'
import RedefinirPaciente from './pages/RedefinirPaciente'

import Conta from './pages/Conta'
import Privacidade from './pages/Privacidade'
import Configuracao from './pages/Configuracao'

import CadastroMedico from './pages/CadastroMedico'
import Edicao_Paciente from './pages/Edicao_Paciente'
import PaginaPaciente from './pages/PaginaPaciente'









function Routes() {
  return (
    <BrowserRouter>
      <Route path="/cadastro_medico" component={CadastroMedico} exact/>
      <Route path="/login" component={Login} exact/>

      <Route path="/" component={PaginaInicial} exact/>
      <Route path="/conta" component={Conta} exact/>
      <Route path="/esqueci_senha" component={EsqueciSenha} exact/>
      <Route path="/redefinir_senha" component={RedefinirPaciente} exact/>
      <Route path="/privacidade" component={Privacidade} exact/>
      <Route path="/configuracao" component={Configuracao} exact/>


      <Route path="/PaginaPaciente" component={PaginaPaciente} exact/>
      <Route path="/cadastro_paciente" component={CadastroPaciente} exact/>
      <Route path="/Edicao_Paciente" component={Edicao_Paciente} exact/>


    </BrowserRouter>
  );
}

export default Routes;