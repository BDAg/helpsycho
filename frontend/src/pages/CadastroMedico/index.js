import React from 'react';

import { Form, 
  Button,
  Col, 
  Row}
  from 'react-bootstrap';

import {Link} from 'react-router-dom'

import logo from '../../assets/images/logo.svg'

import './styles.css';

function PaginaTeste() {
  return (
    <div className="content-medico">
      <div className="quadrado-medico">
        <Col className="colLogo">
            <img src={logo} alt="svg"/>
        </Col>
       
        <Row>
          <Col>
          <Form.Group controlId="formGridName">
              <Form.Label>Nome</Form.Label>
              <Form.Control   />
            </Form.Group>
            <Form.Group controlId="formGridName">
              <Form.Label>Sobrenome</Form.Label>
              <Form.Control  />
            </Form.Group>
            <Form.Group controlId="formGridEmail">
              <Form.Label>E-mail</Form.Label>
              <Form.Control type="email" />
            </Form.Group>
            <Form.Group controlId="formGridPassword">
              <Form.Label>Senha</Form.Label>
              <Form.Control type="password" />
            </Form.Group>
            <Form.Label className="label-radio-button" as="legend" column sm={2}>Sexo</Form.Label>
            <Row > 
            <Form.Check 
              className="radio-button"
              type="radio"
              label="Masculino"
              name="formHorizontalRadios"
              id="formHorizontalRadios1"
              
            />
            <Form.Check
              type="radio"
              label="Feminino"
              name="formHorizontalRadios"
              id="formHorizontalRadios2"
              className="radio-button"
            />
        </Row> 
          </Col>       
        </Row>
        <Row>
          <Col>
            <Button variant="primary" className="button" type="Cadastrar">Cadastrar</Button>
            <p>Você já é Cadastrado ? <Link to="/login">Faça o Login</Link></p>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default PaginaTeste;
