import React from 'react';
import './styles.css';
import { Form, Button, Col, Row, Container } from 'react-bootstrap';
import logo from '../../assets/images/logo.svg';

function Login() {
  return (
    <div className="login">
   
      
      <Row>
      <Container>  <Row className="logo"><img src={logo} alt="svg"/></Row>
      </Container>
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control type="email" placeholder="Enter email" />
            
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" />
          </Form.Group>

          

          <Col>
            <Row>
              <Button variant="primary" type="Login">Login</Button>
            </Row>
          </Col>
        </Form>
      </Row>
    </div>
  );
}

export default Login;
