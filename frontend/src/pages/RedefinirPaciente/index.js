import React from 'react';
import { Form, Button, Col, Row} from 'react-bootstrap';
import './style.css';


import NavB from '../Padrao'

function RedefinirPaciente() {
  return (
 
  <div>

<NavB/>
  <div className="content" tabIndex="-1">
    <div className="quadrado">
      <div className= "formulario">
          <Row>
            
            <div className="redefinir">
            <h2>Redefinir Nome do Paciente</h2>
            </div>
            
          </Row>
          <Row>
            <Col>
            <Form.Group controlId="formGridPassword1">
                <Form.Label>Novo nome do paciente</Form.Label>
                <Form.Control type="text" placeholder="Exemplo: Joana Batista" />
              </Form.Group>
            </Col>
          </Row>


          <div className="botoes">
          <Row>
            <Col>
              <Button variant="primary" type="Redefinir">Redefinir</Button>
            </Col>
            <Col>
              <Button variant="danger" type="Cancelar">Cancelar</Button>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  </div>

</div>
  )
}

export default RedefinirPaciente;