import React from 'react';
import { Form, Button, Col, Row} from 'react-bootstrap';
import user from '../../assets/images/user.svg';


import NavB from '../Padrao'

import './styles.css';

function conta() {
  return (
    <>
    <NavB/>
    <div className="content1" tabIndex="-1">
    <div className="quadrado">
      <Row className="conta">
        <h2>Conta</h2>
      </Row>
      <Col>
      <Row>
      <Col className="colUser">
          <img src={user} alt="svg"/>
        </Col>
        <Col className="mudaImg">
        <Button  variant="primary" >Alterar Imagem</Button>
        </Col>
      </Row>
        <Col className="info">
          <Form.Group controlId="formGridNome">
            <h3>Iago</h3>
          </Form.Group> 
        </Col>
        </Col>
    </div>
  </div>
  </>
);
}

export default conta;