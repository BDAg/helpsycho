import React from 'react';
import { Form, Col, Row, Jumbotron, Container, Dropdown, NavDropdown, Navbar, Nav, Image, Button } from 'react-bootstrap';
import './styles.css';
import user from '../../assets/images/user.svg';
import logo from '../../assets/images/logo.svg';

function Edicao_Paciente() {
  return (

    <>
    <div>
      <Row className="nav">
        <Navbar className="nave" bg="light" expand="lg">
          <Col className="logoMarca">
            <Row >  
              <Image className="logo" src={logo} roundedCircle />
            </Row>
            <Navbar.Brand href="/">HelPsycho</Navbar.Brand>
          </Col>

          <Col>
            <Row>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="itens">
                  <Dropdown className="dropleft">
                  <Dropdown.Toggle className="buttontwo" variant="outline-secondary" id="dropdown-basic">
                  < Image className="user" src={user} roundedCircle />
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item href="#/action-1">Conta</Dropdown.Item>
                    <Dropdown.Item href="#/action-2">Configurações</Dropdown.Item>
                    <Dropdown.Item href="#/action-3">Privacidade</Dropdown.Item>
                    <NavDropdown.Divider />
                    <Dropdown.Item href="#/action-4">Sair</Dropdown.Item>
                  </Dropdown.Menu>
                  </Dropdown>
                </Nav>
              </Navbar.Collapse>
            </Row>
          </Col>
        </Navbar>
      </Row>
    </div>
     <Jumbotron> 
       <Container className="jumbotron-container">
      <div>  <img src={user} alt="svg" />
        <Col><h5>Paciente 01</h5>
        <h6>Idade: 20 </h6> </Col> </div>
        <Button className="botao" variant="primary" type="Adicionar">Adicionar</Button>
      </Container>
      
    </Jumbotron>   
    <div className="content" tabIndex="-1">
      <div className="quadrado-paciente">
        <Row>
          <Col>
            <Form.Group controlId="Paciente 1">
              <Form.Control placeholder="Consulta 01" />
            </Form.Group>
            
            <Form.Group controlId="Paciente 2">
              <Form.Control placeholder="Consulta 02" />
            </Form.Group>
            
            <Form.Group controlId="Paciente 3">
              <Form.Control placeholder="Consulta 03" />
            </Form.Group>

            <Form.Group controlId="Paciente 4">
              <Form.Control placeholder="Consulta 04" />
            </Form.Group>
          </Col>
         </Row>

         
      </div>
    </div>
    
    </>
  );
}

export default Edicao_Paciente;
