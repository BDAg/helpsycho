import React from 'react';
import { Form, Button, Col, Row, Jumbotron, InputGroup} from 'react-bootstrap';

import NavB from '../Padrao'

import './styles.css';

function PaginaTeste() {
  return (
    
  <>
    <NavB/>
    <div className="content-privacidade">
      <div className="quadrado-privacidade">

     
        <Jumbotron className="caixa-box1">
          <Col>
            <Row className="pv">
              <h1>Privacidade</h1>
            </Row>
            <Row>
            <Col>
              <h4>Senha:</h4>
              <InputGroup className="mb-3">
              <Form.Control type="password" />
                <Button variant="outline-primary">Editar</Button>
            </InputGroup>
            </Col>
              
            </Row>
            <Row className="save">
            <Button variant="primary">Salvar Alterações</Button>
            </Row>
          </Col>
        </Jumbotron>
  



      </div>
    </div>
  </>
  );
}

export default PaginaTeste;
