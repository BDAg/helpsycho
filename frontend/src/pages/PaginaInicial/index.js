import React from 'react';

import search from '../../assets/images/search.svg';

import NavB from '../Padrao'

import './pageinicial.css';


import {

  Row,
  Image,
  Col,
  FormControl,
  InputGroup,
  Button,
  Jumbotron,
  ListGroup,
  Dropdown,
  DropdownButton,
  
} from 'react-bootstrap';

function NavBar() {
  return (
    <>
  <NavB/>
  <div className="initial">
    <Col>    
      <div className="pesquisa">
      <InputGroup className="mb-3">
        <FormControl
          placeholder="Nome do paciente"
          aria-label="Recipient's username"
          aria-describedby="basic-addon2"
        />
        <InputGroup.Append>
          <Button variant="outline-secondary">
          <Image className="search" src={search} roundedCircle />
          </Button>
        </InputGroup.Append>
      </InputGroup>
      </div>
    </Col>

  
    <Col>
      <Jumbotron>
      <Row className="novo"> 
            <Button className="botaoNovo" href="/cadastro_paciente">Novo</Button>
        </Row>
        <Col>
          <ListGroup className="clientes">
      
          <ListGroup.Item>
              <Row>
                <Col className="button">
                <ListGroup  defaultActiveKey="#link1">
                        <ListGroup.Item nameClass="cliente" action href="#link2" >
                          Maria Fernanda de Souza
                        </ListGroup.Item>
                      </ListGroup>
                  <DropdownButton variant="outline-secondary" id="dropdown-item-button">
                    <Dropdown.Item as="button">Excluir</Dropdown.Item>
                  </DropdownButton>
                </Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col className="button">
                <ListGroup  defaultActiveKey="#link1">
                        <ListGroup.Item nameClass="cliente" action href="#link2" >
                          Estela Faria Lima
                        </ListGroup.Item>
                      </ListGroup>
                  <DropdownButton variant="outline-secondary" id="dropdown-item-button">
                    <Dropdown.Item as="button">Excluir</Dropdown.Item>
                  </DropdownButton>
                </Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col className="button">
                <ListGroup  defaultActiveKey="#link1">
                        <ListGroup.Item nameClass="cliente" action href="#link2" >
                          Maria Benedita da Cunha
                        </ListGroup.Item>
                      </ListGroup>
                  <DropdownButton variant="outline-secondary" id="dropdown-item-button">
                    <Dropdown.Item as="button">Excluir</Dropdown.Item>
                  </DropdownButton>
                </Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col className="button">
                <ListGroup  defaultActiveKey="#link1">
                        <ListGroup.Item nameClass="cliente" action href="#link2" >
                          Alfredo Marquez da Penha
                        </ListGroup.Item>
                      </ListGroup>
                  <DropdownButton variant="outline-secondary" id="dropdown-item-button">
                    <Dropdown.Item as="button">Excluir</Dropdown.Item>
                  </DropdownButton>
                </Col>
              </Row>
            </ListGroup.Item>
            
            

          </ListGroup>
        </Col>
      </Jumbotron>
    </Col>
  
</div>
</>
   
 );

}

export default NavBar;