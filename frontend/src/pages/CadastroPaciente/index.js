import React from 'react';
import { Form, Button, Col, Row} from 'react-bootstrap';
import user from '../../assets/images/user.svg';

import './styles.css';

function PaginaTeste() {
  return (
    <div className="content" tabIndex="-1">
      <div className="quadrado-paciente">
        <Row>
          <h2>Cadastro do Paciente</h2>
        </Row>

        <Row>
          <Col>
            <Form.Group controlId="formGridAddress1">
              <Form.Label>Nome</Form.Label>
              <Form.Control placeholder="Ex: Milene Silva " />
            </Form.Group>
            <Form.Group controlId="formGridEmail">
              <Form.Label>Data de Nascimento</Form.Label>
              <Form.Control type="Data" placeholder="Ex: 18/09/2001" />
            </Form.Group>
            <Form.Label as="legend" column sm={2}>Sexo</Form.Label>
            <Form.Check
              type="radio"
              label="Masculino"
              name="formHorizontalRadios"
              id="formHorizontalRadios1"
            />
            <Form.Check
              type="radio"
              label="Feminino"
              name="formHorizontalRadios"
              id="formHorizontalRadios2"
            />
          </Col>

          <Col className="colUser">
            <img src={user} alt="svg"/>
          </Col>
        </Row>

        <Row>
          <Col>
            <Button variant="primary" type="Cadastrar">Cadastrar</Button>
          </Col>
          <Col>
            <Button variant="danger" type="Cancelar">Cancelar</Button>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default PaginaTeste;
