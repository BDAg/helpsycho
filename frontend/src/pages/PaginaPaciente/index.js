import React from 'react';
import { Form, Col, Row, Button } from 'react-bootstrap';
import './styles.css';



import NavB from '../Padrao'

function PaginaPaciente() {
  return (

    <>
    <NavB/>
      
    <div className="content" tabIndex="-1">
      <div className="quadrado-paciente">
      <Row>
          <h2 className="h2">Consulta 01</h2>
        </Row>
      
      <Col>
        <Form.Group className="data" controlId="Data">
              <Form.Label>Data</Form.Label>
              <Form.Control  type="Data" placeholder=" " />
        </Form.Group>
        
        <Form.Group className="horario" controlId="Horário">
              <Form.Label>Horário</Form.Label>
              <Form.Control placeholder="" />
        </Form.Group> </Col>
        <Row>
          <Col>
            <Form.Group className="anotacoes" controlId="Anotações">
              <Form.Label  >Anotações:</Form.Label>
              <Form.Control className="caixa"  placeholder=" " />
            </Form.Group>
            
          </Col>
         </Row>

        <Row>
            <Col>
            <Button className="botao" variant="primary" type="Salvar">Salvar</Button>
            </Col>
        </Row>
         
      </div>
    </div>
    
    </>
  );
}

export default PaginaPaciente;
