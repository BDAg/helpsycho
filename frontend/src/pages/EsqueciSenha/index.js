import React from 'react';
import { Form, Button, Col, Row, Dropdown, NavDropdown, Navbar, Nav, Image } from 'react-bootstrap';
import './styleSenha.css';
import user from '../../assets/images/user.svg';
import logo from '../../assets/images/logo.svg';


function EsqueciSenha() {
  return (
 
<div>
  <Row className="nav">
    <Navbar className="nave" bg="light" expand="lg">
      <Col className="logoMarca">
        <Row >  
          <Image className="logo" src={logo} roundedCircle />
        </Row>
        <Navbar.Brand className="titulo" href="#home">HelPsycho</Navbar.Brand>
      </Col>

      <Col>
        <Row>
          <Navbar.Toggle aria-controls="basic-navbar-nav"></Navbar.Toggle>
          <Navbar.Collapse>
            <Nav className="itens">
              <Dropdown className="dropleft">
                <Dropdown.Toggle className="buttontwo" variant="outline-secondary" id="dropdown-basic">
                  <Image className="user" src={user} roundedCircle></Image>
                </Dropdown.Toggle>
         
                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Conta</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">Configurações</Dropdown.Item>
                      <Dropdown.Item href="#/action-3">Privacidade</Dropdown.Item>
                      <NavDropdown.Divider></NavDropdown.Divider>
                      <Dropdown.Item href="#/action-4">Sair</Dropdown.Item>
                    </Dropdown.Menu>
              </Dropdown>
            </Nav>
            </Navbar.Collapse>
        </Row>
      </Col>
    </Navbar>
  </Row>

  <div className="content" tabIndex="-1">
    <div className="quadrado">
      <div className= "formulario">
          <Row>
            
          <div className="redefinir">
          <h2>Redefinir Senha</h2>
          </div>
            
          </Row>
          <Row>
            <Col>
            <Form.Group controlId="formGridPassword1">
                <Form.Label>Nova Senha</Form.Label>
                <Form.Control type="password" placeholder="" />
              </Form.Group>
            <Form.Group controlId="formGridPassword2">
                <Form.Label>Confirme a nova senha</Form.Label>
                <Form.Control type="password" placeholder="" />
              </Form.Group>
            </Col>
          </Row>


          <div className="botoes">
          <Row>
            <Col>
              <Button variant="primary" type="Redefinir">Redefinir</Button>
            </Col>
            <Col>
              <Button variant="danger" type="Cancelar">Cancelar</Button>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  </div>

</div>
  )
}

export default EsqueciSenha;