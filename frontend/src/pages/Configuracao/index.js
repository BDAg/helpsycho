import React from 'react';
import { Form, Button, Col, Row, Jumbotron, InputGroup} from 'react-bootstrap';


import NavB from '../Padrao'


import './styles.css';

function CFG() {
  return (
    
    <>
    <NavB/> 
    <div className="content-privacidade">
      <div className="quadrado-privacidade">
       
       <Row className="priv"> 
       <Jumbotron className="caixa-box1">
       <Col>
         <Row className="pv">
     <h1>Configuração Geral</h1>
     </Row>
        <Row>
            <Col>
              <h4>Nome:</h4>
              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                </InputGroup.Prepend>
                <Form.Control type="name"/>
                <Row> 
            <Col className="itens">
            <Button variant="outline-secundary">Editar</Button>
            </Col>
            </Row>
              </InputGroup>
            </Col>
        </Row>
        <Row>
            <Col>
              <h4>Email:</h4>
              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                </InputGroup.Prepend>
                <Form.Control type="email"/>
                <Row> 
            <Col className="itens">
            <Button variant="outline-secundary">Editar</Button>
            </Col>
            </Row>
              </InputGroup>
            </Col>
        </Row>
        <Row className="save">
        <Button variant="primary">Salvar Altereções</Button>
        </Row>
        </Col>
        </Jumbotron>
       </Row>



      </div>
    </div>
    </>
  );
}

export default CFG;
