import React from 'react';

import { Col, Row, Dropdown, NavDropdown, Navbar, Nav, Image} from 'react-bootstrap';

import './nav.css';
import user from '../../assets/images/user.svg';
import logo from '../../assets/images/logo.svg';


function Padrao() {
  return (

    <>
    <div>
      <Row className="nav">
        <Navbar className="nave" bg="light" expand="lg">
          <Col className="logoMarca">
            <Row >  
              <Image className="logo" src={logo} roundedCircle />
              <Navbar.Brand href="/">HelPsycho</Navbar.Brand>
            </Row>
          </Col>

          <Col>
            <Row>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="itens">
                  <Dropdown className="dropleft">
                  <Dropdown.Toggle className="buttontwo" variant="outline-secondary" id="dropdown-basic">
                  < Image className="user" src={user} roundedCircle />
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item href="/conta">Conta</Dropdown.Item>
                    <Dropdown.Item href="#/action-2">Configurações</Dropdown.Item>
                    <Dropdown.Item href="/privacidade">Privacidade</Dropdown.Item>
                    <NavDropdown.Divider />
                    <Dropdown.Item href="/login">Sair</Dropdown.Item>
                  </Dropdown.Menu>
                  </Dropdown>
                </Nav>
              </Navbar.Collapse>
            </Row>
          </Col>
        </Navbar>
      </Row>
    </div>

    
    </>
  );
}

export default Padrao;
